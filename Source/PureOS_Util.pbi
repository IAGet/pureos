﻿XIncludeFile "PureOS_ProjSettings.pbi"

Procedure.s PureOS_Version() ; версия для обновлялок и т.п (1.0 = 10, 1.4.3=143)
  ProcedureReturn Trim(#PureOS_Version, ".")
EndProcedure

Procedure ReadMainConfig() ; читаем основной конфиг файл
  If OpenPreferences("config.ini")
    PreferenceGroup("Global") ; выбираем группу [Global] для чтения
    PureOS_Options_Language = ReadPreferenceString("Language", "")
    ClosePreferences()
  EndIf
EndProcedure
; IDE Options = PureBasic 5.30 (Windows - x86)
; CursorPosition = 9
; Folding = -
; EnableXP